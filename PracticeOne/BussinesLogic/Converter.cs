﻿namespace BussinesLogic
{
    using System;
    
    public class Converter
    {
        public int [] ConverterStringToArray(string array)
        {
            string [] stringToArray = array.Split(',');
            int[] result = Array.ConvertAll(stringToArray, data => int.Parse(data));
            return result;
        }
    }
}
