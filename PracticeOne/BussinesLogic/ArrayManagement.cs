﻿namespace BussinesLogic
{
    using System;
    using System.Collections.Generic;
 
    public class ArrayManagement
    {
        private List<int> result;
        private Converter converter;
        private Operations operations;

        public ArrayManagement()
        {
            result = new List<int>();
            converter = new Converter();
            operations = new Operations();
        }

        public void NewArray(string first, string second)
        {
            int [] dataOne = converter.ConverterStringToArray(first);
            int [] dataTwo = converter.ConverterStringToArray(second);
            
            if (dataOne.Length == dataTwo.Length)
            {
                for (int i = 0; i < dataOne.Length; i++)
                {
                    result.Add(operations.Sum(dataOne[i], dataTwo[i]));
                }
            }
            else
            {
                Console.WriteLine("The vectors must be the same size.");
            }
        }

        public void AddVector(int data)
        {
            result.Add(data);
        }

        public string Show()
        {
            string array = "[";
            foreach(int i in result)
            {
                array += i + ",";
            }
            array += "]";

            return array;
        }
    }
}
