﻿namespace PracticeOne
{
    using BussinesLogic;
    using System;

    public class Program
    {
        public static void Main(string[] args)
        {
            ArrayManagement arrayManagment = new ArrayManagement();
            
            if (args.Length != 2)
            {
                Console.WriteLine("You need two arrays");
            }
            else
            {
                arrayManagment.NewArray(args[0], args[1]);
            }

            Console.WriteLine($"The result is: {arrayManagment.Show()}");
        }
    }
}
