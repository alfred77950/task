﻿namespace Model.Interface
{
    public interface IOperation
    {
        int Sum(int first, int second);

        int Multiplication(int first, int second);

        double Distance(int first, int second);
    }
}
