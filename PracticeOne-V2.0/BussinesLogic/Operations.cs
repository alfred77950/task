﻿namespace BussinesLogic
{
    using Model.Interface;
    using System;

    public class Operations : IOperation
    {
        public int Sum(int first, int second)
        {
            return first + second;
        }

        public int Multiplication(int first, int second)
        {
            return first * second;
        }

        public double Distance(int first, int second)
        {
            return Math.Pow(second - first, 2);
        }
    }
}