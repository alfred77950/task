﻿namespace BussinesLogic
{
    using System;
    using System.Linq;
    
    public class Converter
    {
        public int[] ConverterStringToArray(string array)
        {
            int convertedNumber;

            int[] arrayInt = array.Split(',').Select(s =>
                int.TryParse(s, out convertedNumber) ? convertedNumber : throw new ArgumentException("The value can't be convert to int.")
            ).ToArray();
            
            return arrayInt;

        }

        public int ConverterStringToInt(string number)
        {
            return int.Parse(number);
        }
    }
}