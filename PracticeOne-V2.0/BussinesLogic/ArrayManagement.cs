﻿namespace BussinesLogic
{
    using System;
    using System.Collections.Generic;
 
    public class ArrayManagement
    {
        private List<int> resultSum;
        private Converter converter;
        private Operations operations;

        public ArrayManagement()
        {
            this.resultSum = new List<int>();
            this.converter = new Converter();
            this.operations = new Operations();
        }

        public void ResultSum(string first, string second)
        {
            int [] dataOne = converter.ConverterStringToArray(first);
            int [] dataTwo = converter.ConverterStringToArray(second);
            
            if (dataOne.Length == dataTwo.Length)
            {
                for (int i = 0; i < dataOne.Length; i++)
                {
                    this.resultSum.Add(operations.Sum(dataOne[i], dataTwo[i]));
                }
            }
            else
            {
                throw new ArgumentException("The vectors must be the same size.");
            }
        }

        public int ResultMultiplication(string first, string second)
        {
            int[] dataOne = converter.ConverterStringToArray(first);
            int[] dataTwo = converter.ConverterStringToArray(second);
            int resultMultiplication = 0;

            if (dataOne.Length == dataTwo.Length)
            {
                for (int i = 0; i < dataOne.Length; i++)
                {
                    resultMultiplication += operations.Multiplication(dataOne[i], dataTwo[i]);
                }
            }
            else
            {
                throw new ArgumentException("The vectors must be the same size.");
            }

            return resultMultiplication;
        }

        public double ResultLenght(string first, string second)
        {
            int[] dataOne = converter.ConverterStringToArray(first);
            int[] dataTwo = converter.ConverterStringToArray(second);
            double resultLength = 0;

            if (dataOne.Length == dataTwo.Length && dataOne.Length == 2)
            {
                for (int i = 0; i < dataOne.Length; i++)
                {
                    resultLength += operations.Distance(dataOne[i], dataTwo[i]);
                }
            }
            else
            {
                throw new ArgumentException("The vectors must be size 2.");
            }

            return Math.Sqrt(resultLength);
        }

        public string GetResulSum()
        {
            string array = "[";
            foreach(int i in this.resultSum)
            {
                array += i + ",";
            }
            array += "]";

            return array;
        }
    }
}
