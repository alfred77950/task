﻿namespace PracticeOne
{
    using BussinesLogic;
    using System;

    public class Program
    {
        public static void Main(string[] args)
        {
            ArrayManagement arrayManagment = new ArrayManagement();

            try
            {
                if (args.Length != 2)
                {
                    Console.WriteLine("You need two arrays");
                }
                else
                {
                    arrayManagment.ResultSum(args[0], args[1]);
                }

                Console.WriteLine($"The result is: {arrayManagment.GetResulSum()}");
            }
            catch (ArgumentException exception)
            {
                Console.WriteLine(exception.Message);
                throw;
            }
        }
    }
}
