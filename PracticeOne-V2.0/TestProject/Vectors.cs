﻿namespace TestProject
{
    using BussinesLogic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;

    [TestClass]
    public class Vectors
    {
        [TestMethod]
        public void Given_TwoVectorsToSum_When_SumIsCorrect_Then_ReturnNewVector()
        {
            ArrayManagement arrayManagement = new ArrayManagement();
            arrayManagement.ResultSum("7,2", "3,-2");
            string result = arrayManagement.GetResulSum();
            string expectedResult = "[10,0,]";

            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        public void Given_TwoVectorsToSum_When_SizeIsDiferente_Then_ReturnExceptionMessage()
        {
            ArrayManagement arrayManagement = new ArrayManagement();
            var exception = Assert.ThrowsException<ArgumentException>(() => arrayManagement.ResultSum("1,2", "3"));
            string expectedResult = "The vectors must be the same size.";

            Assert.AreEqual(expectedResult, exception.Message);
        }

        [TestMethod]
        public void Given_TwoVectorsToSum_When_DataVectorIsNotNumber_ReturnExceptionMessage()
        {
            ArrayManagement arrayManagement = new ArrayManagement();
            var exception = Assert.ThrowsException<ArgumentException>(() => arrayManagement.ResultSum("4,8", "3,dgf"));
            string expectedResult = "The value can't be convert to int.";

            Assert.AreEqual(expectedResult, exception.Message);
        }
       
        [TestMethod]
        public void Given_TwoVectorsToMult_When_MultiplicationIsCorrect_Then_ReturnNumberResult()
        {
            ArrayManagement arrayManagement = new ArrayManagement();
            int resultMultiplication = arrayManagement.ResultMultiplication("4,2", "3,5");
            int expectedResult = 22;

            Assert.AreEqual(expectedResult, resultMultiplication);
        }

        [TestMethod]
        public void Given_TwoVectorsToMultiplication_When_SizeIsDiferente_Then_ReturnExceptionMessage()
        {
            ArrayManagement arrayManagement = new ArrayManagement();
            var exception = Assert.ThrowsException<ArgumentException>(() => arrayManagement.ResultMultiplication("4,8", "3"));
            string expectedResult = "The vectors must be the same size.";

            Assert.AreEqual(expectedResult, exception.Message);
        }

        [TestMethod]
        public void Given_TwoVectorsToMultiplication_When_DataVectorIsNotNumber_ReturnExceptionMessage()
        {
            ArrayManagement arrayManagement = new ArrayManagement();
            var exception = Assert.ThrowsException<ArgumentException>(() => arrayManagement.ResultMultiplication("4,8", "3,dgf"));
            string expectedResult = "The value can't be convert to int.";

            Assert.AreEqual(expectedResult, exception.Message);
        }
        
        [TestMethod]
        public void Given_TwoVectorsToDistance_When_VectorsAreCorrect_Then_ReturnLenghtVector()
        {
            ArrayManagement arrayManagement = new ArrayManagement();
            double resultLength = arrayManagement.ResultLenght("2,1", "-3,2");
            double expectedResult = 5.0990195135927845;

            Assert.AreEqual(expectedResult, resultLength);
        }

        [TestMethod]
        public void Given_TwoVectorsToDistance_When_SizeIsDiferent_Then_ReturnExceptionMessage()
        {
            ArrayManagement arrayManagement = new ArrayManagement();
            var exception = Assert.ThrowsException<ArgumentException>(() => arrayManagement.ResultLenght("4,8", "3"));
            string expectedResult = "The vectors must be size 2.";

            Assert.AreEqual(expectedResult, exception.Message);
        }

        [TestMethod]
        public void Given_TwoVectorsToDistance_When_DataVectorIsNotNumber_ReturnExceptionMessage()
        {
            ArrayManagement arrayManagement = new ArrayManagement();
            var exception = Assert.ThrowsException<ArgumentException>(() => arrayManagement.ResultLenght("-1,-7", "3,aaaa"));
            string expectedResult = "The value can't be convert to int.";

            Assert.AreEqual(expectedResult, exception.Message);
        }

    }
}
